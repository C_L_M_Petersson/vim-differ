" OpenDiff {{{
function! s:OpenDiff()
    if &modified
        let filetype=&filetype
        diffthis

        vnew | r # | normal! ggdd
        diffthis

        exe "setlocal buftype=nofile"
        exe "setlocal bufhidden=wipe"
        exe "setlocal nobuflisted"
        exe "setlocal noswapfile"
        exe "setlocal readonly"
        exe "setlocal filetype=" . filetype

        autocmd BufWipeout <buffer> :call CloseDiff()
    else
        echom "File not modified."
    endif
endfunction
" }}}

" OpenDiff {{{
function! CloseDiff()
    execute "normal! :" . (winnr()-1) . " wincmd w\<CR>"
    diffoff
endfunction
" }}}

" Mappings {{{
noremap <leader>d :call <SID>OpenDiff()<CR>
" }}}
